# mmc-fbsd-bins

FreeBSD binaries for my unofficial MMC builds

### A note about updates

I am and I will be providing 0.6.13 binaries as the newest, because I could not get newer versions to build.

I opened a [issue](https://github.com/MultiMC/Launcher/issues/4365) on MultiMC's github, but nobody responded

I don't think it's an issue (developers said that they don't want anyone to build their product from source), but 0.6.13 is not too bad - supports m$ accounts (so it's usable until möjäng changes the login system once again), forge up to 1.16 (who really uses in on 1.17+?), latest fabric and other modpacks.

### Why

MultiMC doesn't provide BSD binaries officialy, and the source builds are different from official binaries.

### Sources

Sources are here: https://codeberg.org/glowiak/FreeBSD-Ports/src/branch/master/games/multimc

You can compile exactly the same copy of MMC like those binaries with those sources.

### Original MultiMC

Original MultiMC's website: http://multimc.org

This is based on MultiMC.

### Download

Binaries can be downloaded from [releases](https://codeberg.org/glowiak/mmc-fbsd-bins/releases) page.

Sources are in [sources](http://codeberg.org/glowiak/mmc-fbsd-bins#Sources) page.

### What about other BSDs and 32-bit FreeBSD?

As now (state from build 14102021) I'm providing only x86_64 FreeBSD binaries.

Those binaries are built on my host PC. Other binaries I'll compile in VMs.

I'll (try to) compile binaries for OpenBSD, but not for NetBSD. Why? NetBSD is the only BSD OS (maybe except midnightbsd) that doesn't have lwjgl in repos. And there's no way to compile it.

### Binaries compatibility

This page contains things from THE FUTURE!


I'm building x86_64 FreeBSD binaries on FreeBSD 13.0-RELEASE-p3 (REAL PC);

I'm building i686 FreeBSD binaries on FreeBSD 13.0-RELEASE (VM);

I'm building x86_64 OpenBSD binaries on OpenBSD 7.0-SNAPSHOT (VM);

I'm building i686 OpenBSD binaries on OpenBSD 7.0-SNAPSHOT (VM);

### Dependiences

Dependiences:

qt5: GUI

a web browser: micro$hit login

curl: getting IDs

X11 (xorg): GUI

OpenJDK 8: game

OpenJDK 16: Minecraft 1.17+

LWJGL 2: Minecraft up to 1.12.2

LWJGL 3: Minecraft 1.13 and up

### Java runtimes

Alongside binaries there are also java runtimes in runtimes directory. Those runtimes can be used to path with LWJGL.

There are binaries for most FBSD LWJGL implementations: minecraft-client's runtime; Cameron Katri's LWJGL3's runtime; mine minecraft16 runtime;